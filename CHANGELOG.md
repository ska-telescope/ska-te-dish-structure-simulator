# Version History

## Unreleased

* [WOM-655] - Update to CETC release 4.7
* [WOM-598] - Update to CETC release 4.6
* [WOM-535] - Update to CETC release 4.5
* [WOM-508] - Update to CETC release 4.4
* [WOM-493] - Update to CETC release 4.3
* [WOM-481] - Update to CETC release 4.2
* [WOM-425] - Update to CETC release 4.1
* [WOM-407] - Update to CETC release 4.0
* [WOM-398] - Update to CETC release 3.2.3

## 0.11.0

* [REL-1497] - Update to CETC release 3.2.1

## 0.10.0

* [REL-1329] - Update to CETC release 2.3

## 0.9.0

* [WOM-258] - Update to CETC release 2.2

## 0.8.0

* [WOM-245] - Update to CETC release 2.1

## 0.7.0

* [REL-1170] - Bumped to version 2.0 of application
* [AT-1869] - New Dockerfile and folder structure - no more tarball

## 0.6.0

* [REL-1149] - Bumped to new application version
* [AT-1853] - Updates to the pre-release test procedure in Sharepoint reflected in repo
* [AT-1788] - Move dish structure simulator tests from ska-mid-itf to ska-te-dish-structure-simulator.

## 0.5.0

* [REL-1043] - Bumped to new release
* [AT-1756] - Updated with new application version (v1.2)

## 0.4.1

* [REL-1041] - Fixed version number of CETC54 application
* [AT-1743] - Corrected image packaged from CETC54
* [AT-1678] - Add release template
* [AT-1678] - Correct helm chart name.
* [AT-1678] - Fix k8s-test-runner job.

## 0.4.0

* [AT-1443] - v1.1 Dish Structure Sim delivery

## 0.3.0

* [AT-1487] - Remove unused images and helm configuration.

## 0.2.0

* [AT-744] Add Dish structure sim web.
